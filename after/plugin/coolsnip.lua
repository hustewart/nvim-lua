local ls = require("luasnip")
-- ls.parser.parse_snippet.(<text>, <VS**** style snippet)
ls.snippet = {
  all = {
    -- Available in any filetype
    ls.parser.parse_snippet("expand", "-- this got exampended"),
  },

  lua = {
    -- lua specific snippets go here
  },
}
